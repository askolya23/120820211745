import React from 'react'
import Post from "./Posts/Posts"

const Coments = (props) => {

    let commentsElement =
        props.comments.map(c => <Post name={c.name} data={c.data} message={c.message} />);
    let newComment = React.createRef();
    let newName = React.createRef();

    const addComent = () => {
        let text = newComment.current.value;
        let name = newName.current.value;
        newComment.current.value = '';
        newName.current.value = '';
        props.addComent(text, name)
    }

    document.onkeydown = (e) => {
        if (e.ctrlKey && e.key === 'Enter') {
            addComent();
        }
    }

    return (
        <>
            {commentsElement}
            <footer>
                <form>
                    <input type='text' placeholder='Write your name' ref={newName}></input>
                    <textarea placeholder='Write your comment'
                        ref={newComment}></textarea>
                </form>
                <div className='block-button'>
                    <button onClick={addComent}>Написать консультанту</button>
                </div>
            </footer>
        </>
    )
}
export default Coments