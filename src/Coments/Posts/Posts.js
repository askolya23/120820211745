const Post = (props) => {
    return (
        <>
            <div className='wrapper-block-dialog'>
                <div>
                    <div className='person-dialog'>
                        <p className='person-name'>{props.name}</p>
                        <p className='person-data'>{props.data}</p>
                    </div>
                    <div className='person-comment'>
                        <p>{props.message}</p>

                    </div>
                </div>
            </div>
        </>
    )
}
export default Post;