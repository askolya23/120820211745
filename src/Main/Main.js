
const Main = () => {
    return (
        <main>
            <div className='block-reviews'>
                <div className='into-block-reviews'>
                    <p className='last-reviews-text'>Последние отзывы</p>
                    <p><a href='#' >Все отзывы</a></p>
                </div>
                <div className='into-block-reviews reviews-icon'>
                    <p><i className="fa fa-heart"></i>134</p>
                    <p><i className='fas fa-comment-alt'></i>14</p>
                </div>
            </div>
        </main>
    )
}
export default Main