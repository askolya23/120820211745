import './App.css';
import Coments from './Coments/Coments';
import Header from './Header/Header'
import Main from './Main/Main';

function App(props) {

  return (
    <>
      <Header />
      <Main />
      <Coments comments={props.state.comments} addComent={props.addComent} />
    </>
  );
}

export default App;
