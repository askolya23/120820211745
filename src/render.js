import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { addComent } from './redux/state'

export const rerenderEntaireTree = (state) => {
    ReactDOM.render(
        <React.StrictMode>
            <App state={state} addComent={addComent} />
        </React.StrictMode>,
        document.getElementById('root')
    );
}

