

const Header = () => {
    return (
        <header >
            <div className='wrapper-header'>
                <div>
                    <img src='https://rakek92.github.io/works/Consultant/imgs/photo.png'
                        alt='manager' width='125' height='125' className='manager-photo' />
                </div>
                <div className='info-block'>
                    <h1>Вероника Ростова</h1>
                    <p className='manager-text'>Менджер по продажам</p>
                    <div className='info-block-text'>
                        <p>Подберу для вас самые лучшие предложения. Мои услуги абсолютно бесплатны</p>
                    </div>
                    <div className='info-block-services'>
                        <p className='services-text'>Услуг</p>
                        <hr />
                        <div className='info-block-wrapper'>
                            <div className='info-block-services-text'>
                                <p className='info-text-booking'>Ручное бронирование</p>
                                <p>11</p>
                            </div>
                            <div className='info-block-services-text'>
                                <p className='info-text-package'>Пакетные услуги</p>
                                <p>3</p>
                            </div>
                            <div className='info-block-services-text'>
                                <p className='info-text-hostel'>Отели</p>
                                <p>1</p>
                            </div>
                        </div>
                        <hr />
                        <div className='info-block-total'>
                            <p>Всего</p>
                            <p>15</p>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    )
}
export default Header;