
export function getDate() {
    let date = new Date();
    var options = {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
    };
    let mydate = date.toLocaleString("ru", options);
    mydate = mydate.substring(0, mydate.length - 2);
    return mydate
}